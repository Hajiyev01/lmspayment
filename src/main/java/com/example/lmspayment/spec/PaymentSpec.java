package com.example.lmspayment.spec;

import com.example.lmspayment.entity.Student;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class PaymentSpec {

   public static Specification <Student> phoneNumber(String phoneNumber){

       return (Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder builder )->{
           Predicate predicate =builder.equal(root.get("phoneNumber"),phoneNumber);
           return predicate;
       } ;
   }
}
