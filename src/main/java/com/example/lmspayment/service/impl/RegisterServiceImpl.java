package com.example.lmspayment.service.impl;

import com.example.lmspayment.config.Mapper;
import com.example.lmspayment.dto.LoginDto;
import com.example.lmspayment.dto.RegisterDto;
import com.example.lmspayment.entity.Register;
import com.example.lmspayment.repo.RegisterRepository;
import com.example.lmspayment.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
@RequiredArgsConstructor
@Service
public class RegisterServiceImpl implements RegisterService {


    private final Mapper mapper;
    private final RegisterRepository registerRepository;

    @Override
    public void saveRegister(RegisterDto registerDto) {
        Register register = mapper.getMapper().map(registerDto, Register.class);

        registerRepository.save(register);

    }

    @Override
    public boolean login(LoginDto loginDto) {

        Register register = registerRepository.findByUserName(loginDto.getUserName());
        if (register != null && register.getPassword().equals(loginDto.getPassword())) {
            return true;
        }
        return false;
    }}