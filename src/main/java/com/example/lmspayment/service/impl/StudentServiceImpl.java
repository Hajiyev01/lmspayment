package com.example.lmspayment.service.impl;

import com.example.lmspayment.config.Mapper;
import com.example.lmspayment.dto.StudentDto;
import com.example.lmspayment.entity.Student;
import com.example.lmspayment.repo.StudentRepository;
import com.example.lmspayment.service.StudentService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {

    private  final StudentRepository studentRepository;
    private final Mapper mapper;

    @Override
    public void saveStudent(StudentDto studentDto) {
        Student student = mapper.getMapper().map(studentDto,Student.class);
        studentRepository.save(student);

    }

    @Override
    public List<Student> getAll() {
     return  studentRepository.findAll();

    }

    @Override
    public List<Student> getPhoneNumber(String phoneNumber) {
        Specification <Student> specification = findPhoneNumber(phoneNumber);
        return studentRepository.findAll(specification);
    }

    @Override
    public Student update(Long id, Student updateStudent) {
        Student student = studentRepository.findById(id).orElse(null);
        if (student == null) {
            throw new EntityNotFoundException("Student not Found:");
        }
       student.setName(updateStudent.getName());
        student.setSurName(updateStudent.getSurName());
        student.setEmail(updateStudent.getEmail());
        student.setPhoneNumber(updateStudent.getPhoneNumber());
        student.setCourses(updateStudent.getCourses());
        student.setPayments(updateStudent.getPayments());
        return studentRepository.save(student);
    }

    public Specification<Student> findPhoneNumber(String phoneNumber) {
      return   (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("phoneNumber"), phoneNumber);

    }
}
