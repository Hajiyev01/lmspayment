package com.example.lmspayment.service;

import com.example.lmspayment.dto.LoginDto;
import com.example.lmspayment.dto.RegisterDto;
import org.springframework.stereotype.Service;

@Service
public interface RegisterService {
    void saveRegister(RegisterDto registerDto);

    boolean login(LoginDto loginDto);
}
