package com.example.lmspayment.service;

import com.example.lmspayment.dto.StudentDto;
import com.example.lmspayment.entity.Student;

import java.util.List;

public  interface StudentService   {
    void saveStudent(StudentDto studentDto);

    List<Student> getAll();

    List<Student> getPhoneNumber(String phoneNumber);

    Student update(Long id, Student updateStudent);
}
