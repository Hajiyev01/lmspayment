package com.example.lmspayment.repo;

import com.example.lmspayment.entity.Register;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegisterRepository extends JpaRepository<Register,Long> {
    Register findByUserName(String userName);
}
