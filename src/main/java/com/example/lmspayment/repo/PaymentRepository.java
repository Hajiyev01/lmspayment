package com.example.lmspayment.repo;

import com.example.lmspayment.entity.Payment;
import com.example.lmspayment.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentRepository extends JpaRepository<Payment,Long> , JpaSpecificationExecutor<Student> {



}
