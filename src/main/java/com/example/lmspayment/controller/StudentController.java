package com.example.lmspayment.controller;

import com.example.lmspayment.dto.StudentDto;
import com.example.lmspayment.entity.Student;
import com.example.lmspayment.service.PaymentService;
import com.example.lmspayment.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/apis")
public class StudentController {

    private  final StudentService studentService;

    @PostMapping("/student")
    public ResponseEntity<String> saveStudent(@RequestBody StudentDto studentDto){
        studentService.saveStudent(studentDto);
        return  ResponseEntity.ok("Saved successfully");
    }

    @GetMapping
    public List<Student> getAllStudent(){
        return studentService.getAll();
    }
    @GetMapping("/{phoneNumber}")
    public List<Student> getStudentPhoneNumber (@PathVariable String phoneNumber){

        return studentService.getPhoneNumber(phoneNumber);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id,@RequestBody Student updateStudent){
        Student student = studentService.update(id,updateStudent);
        return ResponseEntity.ok(student);
    }
}
