package com.example.lmspayment.controller;

import com.example.lmspayment.dto.LoginDto;
import com.example.lmspayment.dto.RegisterDto;
import com.example.lmspayment.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegisterController {


    private final RegisterService registerService;

    @PostMapping("/register")
    public ResponseEntity<String> saveRegister(@RequestBody RegisterDto registerDto){
        try {
            registerService.saveRegister(registerDto);
            return ResponseEntity.ok("ok");
        }
        catch (Exception e){
            String message= "Xeta Durumu :" + e.getMessage();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message);
        }
    }

    @PostMapping("/login")
    public  ResponseEntity<String> login(@RequestBody LoginDto loginDto){

        boolean isTrue = registerService.login(loginDto);
        if (isTrue){
            return ResponseEntity.ok("ok");

        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Failed:");
        }
    }

}
