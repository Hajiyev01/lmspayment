package com.example.lmspayment.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("http://localhost:3000"); // Herhangi bir kaynaktan isteklere izin vermek için "*"
        config.addAllowedHeader("*"); // Tüm başlıklara izin vermek için "*"
        config.addAllowedMethod("*"); // Tüm HTTP metodlarına izin vermek için "*"
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);

}}