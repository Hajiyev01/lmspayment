package com.example.lmspayment.excep;

public class CourseNotFoundException extends RuntimeException{

    public CourseNotFoundException(String message){
        super (message);
    }
}
