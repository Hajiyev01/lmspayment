package com.example.lmspayment.excep;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AllHandleException {

    @ExceptionHandler(StudentNotFoundException.class)
    public ResponseEntity<String> studentExc(StudentNotFoundException studentNotFoundException){
        return new ResponseEntity<>("Error message:" +studentNotFoundException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CourseNotFoundException.class)
    public ResponseEntity<String> courseExc(CourseNotFoundException courseNotFoundException ){
        return new ResponseEntity<>("Error message:" +courseNotFoundException.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
