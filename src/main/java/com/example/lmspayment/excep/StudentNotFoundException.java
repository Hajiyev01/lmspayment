package com.example.lmspayment.excep;

public class StudentNotFoundException extends RuntimeException{

    public  StudentNotFoundException(String message){
        super(message);
    }
}
